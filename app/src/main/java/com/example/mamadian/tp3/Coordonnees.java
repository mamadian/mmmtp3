package com.example.mamadian.tp3;

/**
 * Created by mamadian on 19/03/16.
 */
public class Coordonnees {

    double longitude;
    double Altitude;
    public Coordonnees(double longi,double alt){
        this.longitude=longi;
        this.Altitude=alt;
    }

    public double getAltitude() {
        return Altitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
