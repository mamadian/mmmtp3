package com.example.mamadian.tp3;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

/**
 * Created by mamadian on 19/03/16.
 */
public class ActiviteDetail extends Activity implements Communicator {
    int index = 0;

    @Override
    public void onCreate(Bundle onsaveState) {
        super.onCreate(onsaveState);
        setContentView(R.layout.detail_activite);
        WebView web = (WebView) findViewById(R.id.web);
        Log.e("Intent", getIntent().getStringExtra("ville"));
        index = Integer.parseInt(getIntent().getStringExtra("index"));
        web.loadUrl("http://www.technoresto.org/vdf/" + getIntent().getStringExtra("ville"));
    }

    @Override
    public void respond(String str, int i) {
        FragmentManager fm = this.getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MapsFragment map = new MapsFragment();
        map.setIndex(index);
        ft.replace(R.id.we, map);
        ft.commit();
    }
}
