package com.example.mamadian.tp3;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by mamadian on 19/03/16.
 */
public class RegionFragment extends Fragment implements AdapterView.OnItemClickListener{
    Communicator com;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        com=(Communicator)getActivity();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstantState){
        View view = inflater.inflate(R.layout.region_fragment,container,false);
        ListView listView=(ListView)view.findViewById(R.id.list);
        listView.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String[] cities = getResources().getStringArray(R.array.TableauRegions);
        com.respond(cities[i].toLowerCase(), i);
    }
}
