package com.example.mamadian.tp3;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamadian on 19/03/16.
 */
public class DetailsFragment  extends Fragment {

    WebView webView;
    List<Coordonnees> cord;
    Button button;
    Communicator com;
    int index=0;
    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        cord=new ArrayList<Coordonnees>();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstantState){
        com=(Communicator)this.getActivity();
        View view = inflater.inflate(R.layout.details_fragment,container,false);
        webView = (WebView)view.findViewById(R.id.web);
        webView.setWebViewClient(new webViewClient());
        webView.loadUrl("https://www.google.com");
        button=(Button)view.findViewById(R.id.localiser);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                com.respond(null,index);
            }
        });
        return view;
    }
    private class webViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public void onAttach (Activity activity){
        super.onAttach(activity);
        com=(Communicator)activity;

    }

    public void naviger(String url){
        webView.loadUrl(url);
    }

}
