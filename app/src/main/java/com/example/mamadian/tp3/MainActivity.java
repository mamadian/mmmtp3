package com.example.mamadian.tp3;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


public class MainActivity extends Activity implements Communicator{
    android.app.FragmentManager fmanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fmanager=getFragmentManager();
    }


    @Override
    public void respond(String str,int i) {
        if(str==null){
           FragmentManager fm=this.getFragmentManager();
            FragmentTransaction ft=fm.beginTransaction();
            MapsFragment map=new MapsFragment();
            map.setIndex(i);
            ft.replace(R.id.detail,map);
            ft.commit();
        }else{
            DetailsFragment fragment=(DetailsFragment)fmanager.findFragmentById(R.id.detail);
            if(fragment!=null && fragment.isVisible()){
                Log.e("main", str);
                fragment.naviger("http://www.technoresto.org/vdf/"+str);
            }else{
                Log.e("Message",str);
                Intent intent=new Intent(this,ActiviteDetail.class);
                intent.putExtra("ville",str);
                intent.putExtra("index",i+"");
                startActivity(intent);
            }
        }
    }

}