package com.example.mamadian.tp3;

import android.os.Bundle;
import android.webkit.WebView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamadian on 19/03/16.
 */
public class MapsFragment extends MapFragment {

    int index;
    List<Coordonnees> coordonnees;
    private GoogleMap mMap;

    public void MapsFragment(){
        index=0;
    }
    WebView webView;


    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        coordonnees =new ArrayList<Coordonnees>();
        coordonnees.add(new Coordonnees(7.4627384f, 48.2899186f));
        coordonnees.add(new Coordonnees(14.6694138f, -61.0942902f));
        coordonnees.add(new Coordonnees(57.9259040f, -16.6468611f));
        coordonnees.add(new Coordonnees(48.0266280f, 0.3332350f));
        coordonnees.add(new Coordonnees(44.7710790f, 5.7428060f));
        coordonnees.add(new Coordonnees(43.5912356f, 3.2583626f));
        coordonnees.add(new Coordonnees(44.4932112f, -0.2340997f));
        coordonnees.add(new Coordonnees(46.8042967f, 2.7959010f));
        coordonnees.add(new Coordonnees(45.7351456f, 4.6108043f));
        coordonnees.add(new Coordonnees(33.5414583, -112.2525593));
        coordonnees.add(new Coordonnees(47.2844157, -0.7349849));
        coordonnees.add(new Coordonnees(5.1573493, 9.3673084));
        coordonnees.add(new Coordonnees(33.5414583, -112.2525593));
        coordonnees.add(new Coordonnees(47.2844157, -0.7349849));
    }

    @Override
    public void onResume (){
        super.onResume();
        localiser(index);
    }
    public void localiser(int i){

        mMap=this.getMap();
        if(mMap!=null)
            mMap.addMarker(new MarkerOptions().position(new LatLng(coordonnees.get(index).getLongitude(), coordonnees.get(index).getAltitude())));
    }
    public void setIndex(int i){
        index=i;
    }
}
